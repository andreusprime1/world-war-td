﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPost : MonoBehaviour
{
    public Transform blockPost;
    public string enemyTag;

    private bool isOpened = false;
    private bool opening = false;
    private float turnAngle = -87;
    private float turnSpeed = 100f;
    float angle = 0f;

    private void Update()
    {
        if (opening)
        {
            StartCoroutine(Open());
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (isOpened)
            return;
        if (other.CompareTag(enemyTag))
        {
            opening = true;
        }

    }
    IEnumerator Open()
    {
        if (angle > turnAngle)
        {
            blockPost.rotation = Quaternion.Euler(0f, angle, 0f);
            angle -= Time.deltaTime*turnSpeed;
            yield return 0;
        }
        else
        {
            opening = false;
            isOpened = true;
            this.enabled = false;
        }
    }
}
