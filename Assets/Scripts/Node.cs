﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public Color hoverColor;
    public int towerIndex = 0;
    public string nodeTag;

    [HideInInspector]
    public GameObject tower;
    [HideInInspector]
    public TowerBlueprint towerBlueprint;
    [HideInInspector]
    public bool isUpgraded = false;

    Renderer rend;
    Color startColor;  

    BuildManager buildManager;

    bool Directed = false;
    Vector3 Offset = new Vector3(0f, .59f, 0f);

    private void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildManager = BuildManager.instance;
    }
    private void Update()
    {
        if (tower != null)
        {
            if (!Directed)
            {
                if (!Input.GetMouseButtonUp(0))
                {
                    tower.transform.rotation = Quaternion.Euler(0f, Input.mousePosition.y, 0f);
                }
                else
                {
                    Directed = true;
                    GameObject _tower = (GameObject)Instantiate(towerBlueprint.prefabs[towerIndex], GetBuildPosition(), tower.transform.rotation);
                    Destroy(tower);
                    tower = _tower;
                    tower.GetComponent<Tower>().treager.Hide();
                    tower.GetComponent<Tower>().node = this;
                }
            }
        }
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + Offset;
    }

    void BuildTower(TowerBlueprint blueprint)
    {
        if (PlayerStats.Money < blueprint.costs[towerIndex])
        {
            //todo: display "not enough money"
            return;
        }
        PlayerStats.Money -= blueprint.costs[towerIndex];
        GameObject _tower = (GameObject)Instantiate(blueprint.emptyPrefab, GetBuildPosition(), Quaternion.identity);
        tower = _tower;
        towerBlueprint = blueprint;
        Directed = false;
    }
    public void UpgradeTower()
    {
        if (PlayerStats.Money < towerBlueprint.costs[towerIndex + 1])
        {
            //todo: display "not enough money to upgrade"
            return;
        }
        PlayerStats.Money -= towerBlueprint.costs[towerIndex + 1];
        //Destroing old tower
        Destroy(tower);
        //Building new one
        GameObject _tower = (GameObject)Instantiate(towerBlueprint.prefabs[towerIndex + 1], GetBuildPosition(), tower.transform.rotation);
        tower = _tower;
        towerIndex++;

        if (towerIndex == towerBlueprint.prefabs.Length)
        {
            isUpgraded = true;
        }
    }
    public void SellTower()
    {
        PlayerStats.Money += towerBlueprint.GetSellAmount(towerIndex);
        Destroy(tower);
        towerBlueprint = null;
    }
    private void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        if (!buildManager.CanBuild)
            return;
        rend.material.color = hoverColor;
    }
    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }
    public void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if(tower != null)
        {
            buildManager.SelectNode(this);
            return;
        }
        if (!buildManager.CanBuild)
            return;
        BuildTower(buildManager.GetTowerToBuild());
    }
}
