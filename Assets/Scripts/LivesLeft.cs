﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class LivesLeft : MonoBehaviour
{
    public Text livesText;

    private void OnEnable()
    {
        StartCoroutine(AnimateText());
    }
    IEnumerator AnimateText()
    {
        livesText.text = "0";
        int live = 0;
        yield return new WaitForSeconds(.7f);
        while (live < PlayerStats.Lives)
        {
            live++;
            livesText.text = live.ToString();
            yield return new WaitForSeconds(0.05f);
        }
    }
}
