﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveList
{
    public string name;
    public float inWaveInterval = 0f;
    public List<Wave> list = new List<Wave>();
}
