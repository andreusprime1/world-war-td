﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(Enemy))]
public class EnemyMovement : MonoBehaviour
{
    public Transform partToRotate;

    private Transform target;
    private int wavePointIndex = 0;

    private Enemy enemy;

    private UnityEngine.AI.NavMeshAgent agent { get; set; }
    private ThirdPersonCharacter character { get; set; }
    public bool Humanoid = false;

    float changePositionDistance;

    private void Start()
    {
        enemy = GetComponent<Enemy>();
        target = WayPoints.wayPoints[0];
        if(Humanoid)
        {
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            changePositionDistance = 3f;

            agent.SetDestination(target.position);
            agent.updateRotation = false;
            agent.updatePosition = true;
            agent.speed = enemy.speed;
        }
        else
        {
            if (enemy.speed >= 10)
            {
                changePositionDistance = 2f;
            }
            else
            {
                changePositionDistance = 0.5f;
            }
        }
    }
    private void Update()
    {
        if (!Humanoid)
        {
            Vector3 dir = target.position - transform.position;
            transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);
            LockOnTarget();
        }
        else
        {
            agent.SetDestination(target.position);
            character.Move(agent.desiredVelocity, false, false);

            agent.stoppingDistance = 0f;
            //AIControl.target = target;
            //gameObject.GetComponent<Animator>().SetTrigger("Run");
        }

        if (Vector3.Distance(transform.position, target.position) < changePositionDistance * Time.timeScale)
        {
            GetNextWayPoint();
        }
    }
    void GetNextWayPoint()
    {
        if (wavePointIndex >= WayPoints.wayPoints.Length - 1)
        {
            EndPath();
            return;
        }
        wavePointIndex++;
        target = WayPoints.wayPoints[wavePointIndex];
    }
    void EndPath()
    {
        PlayerStats.Lives--;
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }
    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime*enemy.turnSpeed).eulerAngles;        
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }
}
