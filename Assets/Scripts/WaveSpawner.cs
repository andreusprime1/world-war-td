﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WaveSpawner : MonoBehaviour
{
    public static int EnemiesAlive = 0;

    public List<WaveList> waves;

    public Transform spawnPoint;

    public float timeBetweenWaves = 5f;
    private float countdown = 10f;

    public Text waveCountdownText;
    public Text waveCounterText;

    public GameManager gameManager;

    private int waveIndex = 0;

    private void Start()
    {
        waveCounterText.text = string.Format("{0} / {1}", waveIndex + 1, waves.Count);
        EnemiesAlive = 0;
    }
    private void Update()
    {
        if (EnemiesAlive > 0)
            return;
        if (waveIndex == waves.Count)
        {
            gameManager.WinLevel();
            this.enabled = false;
        }
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
            return;
        }
        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

        waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }
    IEnumerator SpawnWave()
    {
        waveCounterText.text = string.Format("{0} / {1}", waveIndex + 1, waves.Count);
        PlayerStats.Rounds++;
        EnemiesAlive = GetEnemiesCount(waves[waveIndex]);
        for (int i = 0; i < waves[waveIndex].list.Count; i++)
        {
            for (int j = 0; j < waves[waveIndex].list[i].count; j++)
            {
                SpawnEnemy(waves[waveIndex].list[i].enemy);
                yield return new WaitForSeconds(waves[waveIndex].list[i].interval);
            }
            yield return new WaitForSeconds(waves[waveIndex].inWaveInterval);
        }
        waveIndex++;
    }
    int GetEnemiesCount(WaveList wave)
    {
        int count = 0;
        for(int i = 0; i < wave.list.Count; i++)
        {
            count += wave.list[i].count;
        }
        return count;
    }
    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
    }
}
