﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TowerBlueprint
{
    public GameObject[] prefabs;
    public GameObject emptyPrefab;
    public int[] costs;

    public int GetSellAmount(int index)
    {
        int sellAmount = 0;
        index++;
        for (int i = 0; i < index; i++)
        {
            sellAmount += costs[i];
        }
        sellAmount /= 2;
        return sellAmount;
    }
}
