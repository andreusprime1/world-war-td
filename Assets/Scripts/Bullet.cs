﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Header("Properties")]
    public float speed = 70f;
    public int damage = 50;

    [Header("Effect")]
    public GameObject hitEffect;

    [Header("Mine")]
    public bool mine = false;
    public float explosionRadius = 0f;

    private Transform target;
    private ParabolaController controller;
    float height;
    Vector3 startPosition;

    private void Start()
    {
        if (mine)
        {
            controller = gameObject.GetComponent<ParabolaController>();
            startPosition = transform.position;
            height = Vector3.Distance(transform.position, target.position) - transform.position.y;
            controller.ParabolaRoot.transform.GetChild(0).transform.position = transform.position;
            controller.ParabolaRoot.transform.GetChild(1).transform.position = new Vector3((transform.position.x + target.position.x) / 2,
                (transform.position.y + target.position.y) / 2 + height / 2,
                (transform.position.z + target.position.z) / 2);
            controller.ParabolaRoot.transform.GetChild(2).transform.position = target.position;
            controller.Speed = speed;
        }
    }
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }
        if (mine)
        {
            controller.ParabolaRoot.transform.GetChild(1).position = new Vector3((startPosition.x + target.position.x) / 2,
                (startPosition.y + target.position.y) / 2 + height / 2,
                (startPosition.z + target.position.z) / 2);
            controller.ParabolaRoot.transform.GetChild(2).position = target.position;
            controller.ContinueFollowParabola();
            transform.LookAt(controller.GetNextPoint());
        }
        else
        {
            transform.Translate(dir.normalized * distanceThisFrame, Space.World);
            transform.LookAt(target);
        }
    }

    public void Seek(Transform _target)
    {
        target = _target;
    }

    void HitTarget()
    {
        if (hitEffect != null)
        {
            GameObject effectIns = (GameObject)Instantiate(hitEffect, transform.position, mine ? Quaternion.identity : transform.rotation);
            Destroy(effectIns, 5f);
        }
        if (explosionRadius >0f)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }
        Destroy(gameObject);
    }
    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider collider in colliders)
        {
            if(collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }
    void Damage (Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();
        if(e !=null)
        {
            e.TakeDamage(damage);
        }
    }
}
