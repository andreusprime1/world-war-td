﻿using UnityEngine;
using UnityEngine.UI;

public class NodeUI : MonoBehaviour
{
    public GameObject ui;
    public Text upgradeCost;
    public Text sellAmount;
    public Button upgradeButton;

    //TowerTreager towerTreager;

    private Node target;

    public void SetTarget(Node _target)
    {
        target = _target;

        transform.position = target.GetBuildPosition();

        if (target.towerBlueprint.prefabs.Length == (target.towerIndex + 1))
        {
            upgradeCost.text = "Upgraded!";
            upgradeButton.interactable = false;
        }
        else
        {
            upgradeButton.interactable = true;
            upgradeCost.text = "$" + target.towerBlueprint.costs[target.towerIndex + 1];
        }
        sellAmount.text = "$" + target.towerBlueprint.GetSellAmount(target.towerIndex).ToString();

        ui.SetActive(true);
        target.tower.GetComponent<Tower>().treager.Show();
    }
    public void Hide()
    {
        ui.SetActive(false);
        if (target.tower != null)
        {
            target.tower.GetComponent<Tower>().treager.Hide();
        }
    }
    public void Upgrade()
    {
        target.UpgradeTower();
        BuildManager.instance.DeselectNode();
    }
    public void Sell()
    {
        target.SellTower();
        BuildManager.instance.DeselectNode();
    }
}
