﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTreager : MonoBehaviour
{
    public Tower tower;

    //[HideInInspector]
    public List<Collider> enemies;

    private bool locked = false;
    private int targetIndex = 0;

    private void Start()
    {
        transform.position = new Vector3(transform.position.x, 5, transform.position.z);
    }
    private void Update()
    {
        if (tower.target == null && targetIndex < enemies.Count)
        {
            setNextTarget();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tower.enemyTag))
        {
            enemies.Add(other);
            if (!locked)
            {
                tower.target = other.transform;
                locked = true;
                targetIndex = enemies.Count;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(tower.enemyTag))
        {
            enemies[enemies.IndexOf(other)] = null;
            if (tower.target == other.transform)  //if current target
            {
                tower.target = null;
                locked = false;
                if (targetIndex != enemies.Count)   //if there is another enemy in fire zone 
                {
                    setNextTarget();
                }
            }
        }
    }

    void setNextTarget()
    {
        while (targetIndex < enemies.Count && enemies[targetIndex] == null)
        {
            targetIndex++;
        }
        if (targetIndex == enemies.Count)
            return;
        if (enemies[targetIndex] != null)
        {
            tower.target = enemies[targetIndex].transform;
            locked = true;
        }
    }
    public void Show()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }
    public void Hide()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
}
