﻿using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;
    }

    [HideInInspector]
    public TowerBlueprint towerToBuild;
    public Node selectedNode;

    public NodeUI nodeUI;
    public Shop shop;

    public bool CanBuild { get { return towerToBuild != null; } }
    
    public void SelectNode (Node node)
    {
        if(selectedNode == node)
        {
            DeselectNode();
            return;
        }
        if(selectedNode != null)
        {
            selectedNode.tower.GetComponent<Tower>().treager.Hide();
        }

        selectedNode = node;
        towerToBuild = null;

        nodeUI.SetTarget(node);
    }
    public void DeselectNode()
    {
        if (selectedNode.tower != null)
        {
            selectedNode.tower.GetComponent<Tower>().treager.Hide();
        }
        selectedNode = null;
        nodeUI.Hide();
    }
    public void SelectTowerToBuild(TowerBlueprint tower)
    {
        towerToBuild = tower;
        if (selectedNode != null)
        {
            DeselectNode();
        }
    }
    public TowerBlueprint GetTowerToBuild()
    {
        return towerToBuild;
    }
}
