﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float speed = 10f;
    [HideInInspector]
    public float turnSpeed;

    public float startHealth = 100;
    private float health;
    private bool isDead = false;

    public int value = 40;

    [Header("Unity Stuff")]
    public Image healthBar;
    public GameObject destroyEffect;

    private void Start()
    {
        health = startHealth;
        turnSpeed = speed / 4;
    }
    public void TakeDamage(int amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;
        if (health <=0)
        {
            Die();
            isDead = true;
        }
    }
    void Die()
    {
        if(!isDead)
        {
            PlayerStats.Money += value;
            WaveSpawner.EnemiesAlive--;
            Destroy(gameObject);
            GameObject effectIns = (GameObject)Instantiate(destroyEffect, transform.position, Quaternion.identity);
            Destroy(effectIns, 4f);
        }
    }
}
