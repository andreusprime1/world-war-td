﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComleteLevel : MonoBehaviour
{
    public SceneFader sceneFader;
    public string menuSceneName = "MainMenu";
    public string nextLevel = "Level_2";
    public int curLevelNumber = 1;
    public int levelToUnlock = 2;

    public void Continue()
    {
        if(PlayerPrefs.GetInt("levelReached") == curLevelNumber)
        {
            PlayerPrefs.SetInt("levelReached", levelToUnlock);
        }
        sceneFader.FadeTo(nextLevel);
    }
    public void Menu ()
    {
        sceneFader.FadeTo(menuSceneName);
    }
}
