﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string levelToLoad = "Level_1";

    public SceneFader sceneFader;
    public GameObject settingsUI;
    public Button resetButton;

    private void Start()
    {
        if (PlayerPrefs.GetInt("levelReached") == 1)
        {
            resetButton.interactable = false;
        }
        else
        {
            resetButton.interactable = true;
        }
    }
    public void SettingsToggle()
    {
        settingsUI.SetActive(!settingsUI.activeSelf);
    }
    public void ResetGame()
    {
        PlayerPrefs.SetInt("levelReached", 1);
        resetButton.interactable = false;
    }
    public void Play()
    {
        SceneManager.LoadScene(levelToLoad);
        sceneFader.FadeTo(levelToLoad);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
