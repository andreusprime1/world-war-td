﻿using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public TowerBlueprint standartTower;
    public TowerBlueprint missileLouncher;
    public TowerBlueprint mortarTower;

    [Header("Sprites")]
    public Sprite standardImg;
    public Sprite missileImg;
    public Sprite mortarImg;

    [Header("Buttons")]
    public Button standardButton;
    public Button missileButton;
    public Button mortarButton;

    bool standardSwitched = false;
    bool missileSwitched = false;
    bool mortarSwitched = false;

    BuildManager buildManager;

    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    private void Update()
    {
        if (buildManager.towerToBuild == null && (standardSwitched || missileSwitched || mortarSwitched))
        {
            if (standardSwitched)
            {
                switchImg("standard");
                return;
            }
            if(missileSwitched)
            {
                switchImg("missile");
                return;
            }
            if (mortarSwitched)
            {
                switchImg("mortar");
                return;
            }
        }
    }

    public void SelectStandartTower()
    {
        if(buildManager.GetTowerToBuild() != standartTower)
        {
            if (missileSwitched || mortarSwitched)
            {
                if (missileSwitched)
                {
                    switchImg("missile");
                }
                if (mortarSwitched)
                {
                    switchImg("mortar");
                }
            }
            switchImg("standard");
            buildManager.SelectTowerToBuild(standartTower);
        }
        else
        {
            switchImg("standard");
            buildManager.SelectTowerToBuild(null);
        }
    }
    public void SelectMissileLauncher()
    {
        if (buildManager.GetTowerToBuild() != missileLouncher)
        {
            if (standardSwitched || mortarSwitched)
            {
                if (standardSwitched)
                {
                    switchImg("standard");
                }
                if (mortarSwitched)
                {
                    switchImg("mortar");
                }
            }
            switchImg("missile");
            buildManager.SelectTowerToBuild(missileLouncher);
        }
        else
        {
            switchImg("missile");
            buildManager.SelectTowerToBuild(null);
        }
    }
    public void SelectMortarTower()
    {
        if (buildManager.GetTowerToBuild() != mortarTower)
        {
            if (missileSwitched || standardSwitched)
            {
                if (missileSwitched)
                {
                    switchImg("missile");
                }
                if (standardSwitched)
                {
                    Debug.Log("switching standard");
                    switchImg("standard");
                }
            }
            switchImg("mortar");
            buildManager.SelectTowerToBuild(mortarTower);
        }
        else
        {
            switchImg("standard");
            buildManager.SelectTowerToBuild(null);
        }
    }

    void switchImg(string code)
    {
        Sprite temp;
        if(code == "standard")
        {
            temp = standardButton.image.overrideSprite;
            standardButton.image.overrideSprite = standardImg;
            standardImg = temp;
            standardSwitched = !standardSwitched;
            return;
        }
        if(code == "missile")
        {
            temp = missileButton.image.overrideSprite;
            missileButton.image.overrideSprite = missileImg;
            missileImg = temp;
            missileSwitched = !missileSwitched;
            return;
        }
        if(code == "mortar")
        {
            temp = mortarButton.image.overrideSprite;
            mortarButton.image.overrideSprite = mortarImg;
            mortarImg = temp;
            mortarSwitched = !mortarSwitched;
            return;
        }
    }
}
